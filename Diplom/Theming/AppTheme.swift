//
//  AppTheme.swift
//  Diplom
//
//  Created by Константин Малахов on 27.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

struct AppTheme {
    var statusBarStyle: UIStatusBarStyle
    var barBackgroundColor: UIColor
    var barForegroundColor: UIColor
    var backgroundColor: UIColor
    var textColor: UIColor
}

extension AppTheme {
    static let light = AppTheme(
        statusBarStyle: .`default`,
        barBackgroundColor: .white,
        barForegroundColor: .black,
        backgroundColor: .white,
        textColor: .black
    )
    
    static let dark = AppTheme(
        statusBarStyle: .lightContent,
        barBackgroundColor: UIColor(white: 0, alpha: 1),
        barForegroundColor: .white,
        backgroundColor: .black,
        textColor: .white
    )
}
