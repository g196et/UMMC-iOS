//
//  YearTagCollectionViewCell.swift
//  Diplom
//
//  Created by Константин Малахов on 17.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class YearTagCollectionViewCell: UICollectionViewCell {

    @IBOutlet var yearButton: UIButton!
    
    override func awakeFromNib() {
        self.yearButton.layer.cornerRadius = 10
        self.yearButton.clipsToBounds = true
    }
}
