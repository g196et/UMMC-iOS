//
//  IssueViewController.swift
//  Diplom
//
//  Created by g196et on 14.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class IssueViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    var issues = [Issue]()
    @IBOutlet weak var tableView: UITableView!
    let releasesSegueIdentifier = "EditionToReleasesSegue"
    let topTitle = "Издания"
    
    // MARK: - JSON
    let URL_CONST = "http://kolalola-001-site1.btempurl.com/edition/geteditions"
    
    //this function is fetching the json from URL
    func getJsonFromUrl(){
        //creating a NSURL
        guard let url = URL(string: self.URL_CONST) else { return }
        let sem = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            if let dictionary = json as? [String: Any] {
                for issue_json in dictionary["editions"] as! NSArray {
                    let issue_data = issue_json as! NSDictionary
                    let id = issue_data["id"] as! Int
                    let name = issue_data["name"] as! String
                    let logoUrl = "http://kolalola-001-site1.btempurl.com" + (issue_data["logo"] as! String)
                    let url = URL(string: logoUrl)
                    self.issues.append(Issue(
                            id: id,
                            name: name,
                            logo: url!
                        )
                    )
                }
            }
            sem.signal()
        })
        task.resume()
        sem.wait()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self as UITableViewDelegate
        self.tableView.dataSource = self as UITableViewDataSource
        self.tabBarItem.selectedImage = #imageLiteral(resourceName: "ic_subscriptions_pressed")
        getJsonFromUrl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.topItem?.title = topTitle
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == self.releasesSegueIdentifier,
            let destination = segue.destination as? IssueInformationViewController,
            let cellIndex = tableView.indexPathForSelectedRow?.row
            {
                let edition = self.issues[cellIndex]
                destination.idEdition = edition.id
            }
    }
 
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.issues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "IssueTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                       for: indexPath) as? IssueTableViewCell  else {
                                                        fatalError("The dequeued cell is not an instance of IssueTableViewCell.")
        }
        let issue = self.issues[indexPath.row] as Issue
        let data = try? Data(contentsOf : issue.logo)
        let image = UIImage(data : data!)
        cell.id = issue.id
        cell.logo.image = image!
        if (GlobalSettings.sharedManager.editions.contains(issue.id)){
            cell.editionSwitch.isOn = true
        }
        else{
            cell.editionSwitch.isOn = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }

}
