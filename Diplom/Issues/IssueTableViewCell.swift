//
//  IssueTableViewCell.swift
//  Diplom
//
//  Created by g196et on 14.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class IssueTableViewCell: UITableViewCell {
    var id: Int!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var editionSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func changeEditionsArray(_ sender: UISwitch) {
        if (self.editionSwitch.isOn == true){
            GlobalSettings.sharedManager.editions.append(self.id)
        }
        else{
            if let index = GlobalSettings.sharedManager.editions.index(of: self.id){
                GlobalSettings.sharedManager.editions.remove(at: index)
            }
        }
    }
}
