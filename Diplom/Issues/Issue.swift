//
//  Issue.swift
//  Diplom
//
//  Created by g196et on 14.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import Foundation
import UIKit

struct Issue: Decodable {
    var id: Int
    var name: String
    var logo: URL
    
    init(id: Int, name: String, logo: URL){
        self.id = id
        self.name = name
        self.logo = logo
    }
}
