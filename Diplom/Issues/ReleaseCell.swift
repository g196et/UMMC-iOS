//
//  ReleaseCell.swift
//  Diplom
//
//  Created by Константин Малахов on 26.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class ReleaseCell: UICollectionViewCell {
    
    @IBOutlet var imageRelease: UIImageView!
    @IBOutlet var number: UILabel!
    @IBOutlet var date: UILabel!
}
