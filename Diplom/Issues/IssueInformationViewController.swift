//
//  IssueInformationViewController.swift
//  Diplom
//
//  Created by Константин Малахов on 17.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class IssueInformationViewController: UIViewController, UICollectionViewDataSource {
    
    // MARK: - Properties
    @IBOutlet var myCollectionView: UICollectionView!
    let CELL_WIDTH = CGFloat(integerLiteral: 149)
    let CELL_HEIGHT = CGFloat(integerLiteral: 207)
    let IMAGE_HEIGHT = CGFloat(integerLiteral: 152)
    var years = [2018, 2017, 2016, 2015]
    var selectYears = [Int]()
    var allReleases = [ReleaseShort]()
    var selectedReleases = [ReleaseShort]()
    var idEdition = Int()
    let releaseSegueIdentifier = "ReleasesToReleaseSegue"
    let topTitle = "Выпуски"
    
    // MARK: - JSON
    let URL_CONST = "http://kolalola-001-site1.btempurl.com/edition/getreleases?id="
    
    func getJsonFromUrl(id: Int){
        //creating a NSURL
        guard let url = URL(string: self.URL_CONST + String(id)) else { return }
        let sem = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            if let dictionary = json as? [String: Any] {
                for release in dictionary["releases"] as! NSArray {
                    let dict = release as! NSDictionary
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:m:ss"
                    let formatingDate = dateFormatter.date(from: dict["date"] as! String)
                    self.allReleases.append(
                        ReleaseShort(
                            id: dict["id"] as! Int,
                            number: dict["number"] as! String,
                            date: formatingDate!,
                            image: dict["image"] as? String
                        )
                    )
                }
            }
            sem.signal()
        })
        task.resume()
        sem.wait()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myCollectionView.dataSource = self
        getJsonFromUrl(id: self.idEdition)
        selectedReleases = allReleases
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = topTitle
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == self.releaseSegueIdentifier,
            let destination = segue.destination as? ReleaseViewController,
            let cellIndex = myCollectionView.indexPathsForSelectedItems
        {
            let edition = self.allReleases[(cellIndex.first?.row)!]
            destination.idRelease = edition.id
        }
    }
    
    // MARK: - UICollectionView methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return years.count
        }
        else{
            return selectedReleases.count
        }
    }
    
    @objc func buttonClicked(sender: UIButton) {
        let indexYear = self.selectYears.index(of: Int(sender.title(for: .normal)!)!)
        if indexYear != nil{
            sender.isHighlighted = false
            self.selectYears.remove(at: indexYear!)
        }
        else{
            sender.isHighlighted = true
            self.selectYears.append(Int(sender.title(for: .normal)!)!)
        }
        selectedReleases.removeAll()
        for (_, release) in allReleases.enumerated(){
            let stringFormatter = DateFormatter()
            stringFormatter.dateFormat = "yyyy"
            let dateRelease = stringFormatter.string(from: release.date)
            let checkYear = self.selectYears.index(of: Int(dateRelease)!)
            if checkYear != nil{
                selectedReleases.append(release)
            }
        }
        myCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (indexPath.section == 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IssueYearTagCell", for: indexPath) as! YearTagCollectionViewCell
            cell.yearButton.setTitle(String(years[indexPath.item]), for: .normal)
            cell.yearButton.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReleaseCell", for: indexPath) as! ReleaseCell
            if (indexPath.row % 2 == 0) {
                cell.frame = CGRect(x: cell.frame.minX, y: cell.frame.minY, width: 149, height: 207)
            }
            else{
                cell.frame = CGRect(x: cell.frame.minX + 50, y: cell.frame.minY, width: 149, height: 207)
            }
            NSLayoutConstraint(
                item: cell.imageRelease, attribute: .top, relatedBy: .equal, toItem: cell,
                attribute: .top, multiplier: 1, constant: 0
                ).isActive = true
            NSLayoutConstraint(
                item: cell.imageRelease, attribute: .width, relatedBy: .equal, toItem: nil,
                attribute: .width, multiplier: 1, constant: 149
                ).isActive = true
            NSLayoutConstraint(
                item: cell.imageRelease, attribute: .height, relatedBy: .equal, toItem: nil,
                attribute: .height, multiplier: 1, constant: 152
                ).isActive = true
            NSLayoutConstraint(
                item: cell.imageRelease, attribute: .centerX, relatedBy: .equal, toItem: cell,
                attribute: .centerX, multiplier: 1, constant: 0
                ).isActive = true
            NSLayoutConstraint(
                item: cell.number, attribute: .top, relatedBy: .equal, toItem: cell.imageRelease,
                attribute: .bottom, multiplier: 1, constant: 8
                ).isActive = true
            NSLayoutConstraint(
                item: cell.number, attribute: .centerX, relatedBy: .equal, toItem: cell,
                attribute: .centerX, multiplier: 1, constant: 0
                ).isActive = true
            NSLayoutConstraint(
                item: cell.number, attribute: .bottom, relatedBy: .equal, toItem: cell.date,
                attribute: .top, multiplier: 1, constant: 5
                ).isActive = true
            NSLayoutConstraint(
                item: cell.date, attribute: .centerX, relatedBy: .equal, toItem: cell,
                attribute: .centerX, multiplier: 1, constant: 0
                ).isActive = true
            NSLayoutConstraint(
                item: cell.date, attribute: .bottom, relatedBy: .equal, toItem: cell,
                attribute: .bottom, multiplier: 1, constant: 0
                ).isActive = true
            let release = self.selectedReleases[indexPath.item]
            if release.image != nil {
                let imageUrl = URL(string: ("http://kolalola-001-site1.btempurl.com" + release.image!))
                let data = try? Data(contentsOf : imageUrl!)
                if data != nil{
                    cell.imageRelease.image = UIImage(data : data!)
                }
                else{
                    cell.imageRelease.image = #imageLiteral(resourceName: "default")
                }
            }
            else{
                cell.imageRelease.image = #imageLiteral(resourceName: "default")
            }
            cell.number.text = "№ " + release.number
            cell.number.sizeToFit()
            let stringFormatter = DateFormatter()
            stringFormatter.dateFormat = "dd.MM.yyyy"
            cell.date.text = stringFormatter.string(from: release.date)
            cell.date.sizeToFit()
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
