//
//  PostViewController.swift
//  Diplom
//
//  Created by g196et on 12.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class PostViewController: UIViewController {
    // MARK: - Proprties
    var idPost = Int()
    var article_dict: Dictionary = [String: String]()
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var viewsCount: UILabel!
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet var browserButton: UIButton!
    @IBOutlet var sharedButton: UIButton!
    @IBOutlet var editionButton: UIButton!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var articleView: ArticleView!
    
    func startIndicator(){
        self.indicator.startAnimating()
        self.view.bringSubview(toFront: self.indicator)
    }
    
    func stopIndicator(){
        self.indicator.stopAnimating()
        self.indicator.hidesWhenStopped = true
    }
    
    
    @IBAction func openBrowserLink(_ sender: UIButton) {
        let url = URL(string: self.article_dict["link"]!)
        UIApplication.shared.open(url!)
    }
    
    @IBAction func share(_ sender: UIButton) {
        let activityVC = UIActivityViewController(activityItems: [self.article_dict["link"] as Any], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        
        self.present(activityVC, animated: true, completion: nil )
    }
    
    // MARK: - JSON
    let URL_CONST = "http://kolalola-001-site1.btempurl.com/news/getarticle?id="
    
    //this function is fetching the json from URL
    func getJsonFromUrl(id: Int){
        //creating a NSURL
        guard let url = URL(string: self.URL_CONST + String(id)) else { return }
        let sem = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            if let dictionary = json as? [String: Any] {
                if let article = dictionary["article"] as? NSDictionary {
                    self.article_dict["name"] = (article["name"] as! String)
                    self.article_dict["image"] = "http://kolalola-001-site1.btempurl.com" + (article["image"] as! String)
                    self.article_dict["releaseNumber"] = (article["releaseNumber"] as! String)
                    self.article_dict["editionName"] = (article["editionName"] as! String)
                    self.article_dict["text"] = (article["text"] as! String)
                    self.article_dict["date"] = (article["date"] as! String)
                    self.article_dict["link"] = (article["link"] as! String)
                    self.article_dict["views"] = String(describing: article["views"]!)
                }
            }
            sem.signal()
        })
        task.resume()
        sem.wait()
    }
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startIndicator()
        getJsonFromUrl(id: self.idPost)
        if let navigationBar = self.navigationController?.navigationBar {
            let secondFrame = CGRect(x: navigationBar.frame.width/4*3-10, y: 0, width: navigationBar.frame.width/8, height: navigationBar.frame.height)
            let thirdFrame = CGRect(x: navigationBar.frame.width/8*7, y: 0, width: navigationBar.frame.width/8, height: navigationBar.frame.height)
            
            browserButton.frame = secondFrame
            browserButton.layer.cornerRadius = 20
            browserButton.clipsToBounds = true
            sharedButton.frame = thirdFrame
            sharedButton.layer.cornerRadius = 20
            sharedButton.clipsToBounds = true
            
            navigationBar.addSubview(browserButton)
            navigationBar.addSubview(sharedButton)
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:m:ss.SSSSSS"
        let stringFormatter = DateFormatter()
        stringFormatter.dateFormat = "dd.MM.yyyy"
        let imageUrl = URL(string: self.article_dict["image"]!)
        let data = try? Data(contentsOf : imageUrl!)
        self.imagePost.image = UIImage(data : data!)
        self.viewsCount.text = self.article_dict["views"]
        self.titleLabel.text = self.article_dict["name"]
        self.editionButton.layer.cornerRadius = 10
        self.editionButton.clipsToBounds = true
        self.editionButton.setTitle(self.article_dict["editionName"]! + " №" + self.article_dict["releaseNumber"]! + " >", for: .normal)
        let formatingDate = dateFormatter.date(from: self.article_dict["date"]!)
        self.postDate.text = stringFormatter.string(from: formatingDate!)
        if let data = self.article_dict["text"]?.data(using: String.Encoding.unicode) {
            try? self.textLabel.attributedText = NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            if GlobalSettings.sharedManager.bigTextSize {
                self.textLabel.font = UIFont.systemFont(ofSize: 22)
            } else {
                self.textLabel.font = UIFont.systemFont(ofSize: 17)
            }
            if GlobalSettings.sharedManager.isDarkMode {
                self.textLabel.textColor = .white
            } else {
                self.textLabel.textColor = .black
            }
            
            // self.textLabel.text = self.article_dict["text"]!.htmlToString
        }
        self.titleLabel.sizeToFit()
        self.textLabel.sizeToFit()
        self.tabBarItem.selectedImage = #imageLiteral(resourceName: "ic_home_pressed")
        stopIndicator()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        sharedButton.removeFromSuperview()
        browserButton.removeFromSuperview()
        upViewsCounts(id: self.idPost)
    }
    
    func upViewsCounts(id: Int) {
        // Set up the URL request
        let todoEndpoint: String = "http://kolalola-001-site1.btempurl.com/news/addview?id="
        guard let url = URL(string: todoEndpoint + String(id)) else { return }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print(error!)
                return
            }
        }
        task.resume()
    }
}
