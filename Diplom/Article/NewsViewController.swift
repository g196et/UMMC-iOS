//
//  NewsViewController.swift
//  Diplom
//
//  Created by g196et on 12.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit
import UserNotifications

class NewsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: Properties
    var editions = [Int]()
    var news = [Post]()
    @IBOutlet var generalView: GeneralView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var indicator: UIActivityIndicatorView!
    let FeedTableViewCell = "FeedTableViewCell"
    let postSegueIdentifier = "PostSegue"
    let cellSpacingHeight: CGFloat = 10
    let topTitle = "Новости УГМК"
    
    
    func startIndicator(){
        self.indicator.startAnimating()
        self.tableView.isHidden = true
        self.view.bringSubview(toFront: self.indicator)
    }
    
    func stopIndicator(){
        self.tableView.reloadData()
        self.indicator.stopAnimating()
        self.indicator.hidesWhenStopped = true
        self.tableView.isHidden = false
    }
    
    //this function is fetching the json from URL
    func getJsonFromUrl(editionsArray: [Int]){
        let editions_string = editionsArray.map(String.init).joined(separator: ",")
        let url = NSURLComponents(string: "http://kolalola-001-site1.btempurl.com/news/getarticles")!
        
        var items = [URLQueryItem]()
        
        items.append(URLQueryItem(name: "editions", value: editions_string))
        
        items = items.filter{!$0.name.isEmpty}
        
        if !items.isEmpty {
            url.queryItems = items
        }
        self.editions = editionsArray
        var urlRequest = URLRequest(url: url.url!)
        urlRequest.httpMethod = "GET"
        
        self.startIndicator()

        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            self.news.removeAll()
            if let dictionary = json as? [String: Any] {
                for article in dictionary["articles"] as! NSArray {
                    let dict_art = article as! NSDictionary
                    let id = dict_art["id"] as! Int
                    let name = dict_art["name"] as! String
                    var url_string  = ""
                    // Если изображение не дёрнули от сервера, то вставляем заглушечное изображение.
                    if dict_art["image"] != nil {
                        url_string = "http://kolalola-001-site1.btempurl.com" + (dict_art["image"] as! String)
                    }
                    else{
                        url_string = "http://kolalola-001-site1.btempurl.com/UploadImages/1037.jpeg"
                    }
                    let url = URL(string: url_string)
                    let imageData = try? Data(contentsOf : url!)
                    let releaseNumber = dict_art["releaseNumber"] as! String
                    let editionName = dict_art["editionName"] as! String
                    self.news.append(Post(id: id,
                                          title: name,
                                          image: imageData!,
                                          releaseNumber: releaseNumber,
                                          editionName: editionName
                        )
                    )
                }
            }
             DispatchQueue.main.async {
                self.stopIndicator()
             }
        })
        task.resume()
    }
    
    private func executeRepeatedly() {
        let url = NSURLComponents(string: "http://kolalola-001-site1.btempurl.com/notifications/getpush?deviceId=yjkcvwsd52sd321")!
        var urlRequest = URLRequest(url: url.url!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            if let dictionary = json as? [String: Any] {
                if let notification = dictionary["notification"] as? NSDictionary {
                    let content = UNMutableNotificationContent()
                    content.title = "Новая статья"
                    content.body = notification["articleName"] as! String
                    content.sound = UNNotificationSound.default()
                    content.badge = 1
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5.0, repeats: false)
                    let request = UNNotificationRequest(identifier: "TestIdentifier", content: content, trigger: trigger)
                    UNUserNotificationCenter.current().delegate = self
                    UNUserNotificationCenter.current().add(request) { (error) in
                        if error != nil{
                            print("error \(String(describing: error))")
                        }
                    }
                }
            }
        })
        task.resume()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) { [weak self] in
            self?.executeRepeatedly()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self as UITableViewDelegate
        tableView.dataSource = self as UITableViewDataSource
        executeRepeatedly()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.topItem?.title = topTitle
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !(self.editions.containsSameElements(as: GlobalSettings.sharedManager.editions)){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
                self.getJsonFromUrl(editionsArray: GlobalSettings.sharedManager.editions.sorted())
            }
        }
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == self.postSegueIdentifier,
            let destination = segue.destination as? PostViewController,
            let cellIndex = tableView.indexPathForSelectedRow?.section
        {
            let post = self.news[cellIndex]
            destination.idPost = post.id
        }
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.news.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: cellSpacingHeight))
        footerView.backgroundColor = tableView.backgroundColor
        return footerView
    }
    
    // Make the background color show through
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = UIView()
//        headerView.backgroundColor = UIColor.clear
//        return headerView
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PostTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                       for: indexPath) as? PostTableViewCell  else {
                                                        fatalError("The dequeued cell is not an instance of PostTableViewCell.")
        }
        let post = self.news[indexPath.section]
        cell.titleLabel.text = post.title
        let image = UIImage(data : post.image)
        cell.previewImage.image = image!
        cell.editionLabel.text = post.editionName + " №" + post.releaseNumber
        cell.titleLabel.sizeToFit()
        cell.editionLabel.sizeToFit()
        cell.layer.cornerRadius = 30
        cell.layer.masksToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }

}

extension NewsViewController: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("userNotificationCenter!")
        completionHandler([.alert])
    }
    
}
