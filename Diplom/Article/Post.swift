//
//  Post.swift
//  Diplom
//
//  Created by g196et on 05.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import Foundation
import UIKit

struct Post: Decodable {
    var id: Int
    var title: String
    var image: Data
    var releaseNumber: String
    var editionName: String
    
    init(id: Int, title: String, image: Data, releaseNumber: String, editionName: String){
        self.id = id
        self.title = title
        self.image = image
        self.releaseNumber = releaseNumber
        self.editionName = editionName
    }
}

struct PostShort: Decodable {
    var id: Int
    var name: String
    var image: String
    
    init(id: Int, name: String, image: String){
        self.id = id
        self.name = name
        self.image = image
    }
}
