//
//  AppDelegate.swift
//  Diplom
//
//  Created by g196et on 18.02.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "MySettings")
        fetch.fetchLimit = 1
        let fetchSettings = try! managedContext.fetch(fetch)
        let sem = DispatchSemaphore(value: 0)
        if fetchSettings.count > 0{
            print("READ SETTINGS")
            let settings: MySettings = fetchSettings.first as! MySettings
            GlobalSettings.sharedManager.editions = settings.issues as! Array
            GlobalSettings.sharedManager.currentUser = settings.currentUser
            GlobalSettings.sharedManager.bigTextSize = settings.bigTextSize
            
            if settings.isDarkMode{
                Theme.darkTheme(sem: sem)
                sem.wait()
            }
            else{
                Theme.defaultTheme(sem: sem)
                sem.wait()
            }
            if settings.bigTextSize{
                TextLabel.appearance().font = UIFont.systemFont(ofSize: 22)
            }
            else {
                TextLabel.appearance().font = UIFont.systemFont(ofSize: 17)
            }
        }
        else{
            print("CREATE SETTINGS")
            GlobalSettings.sharedManager.editions = [Int]()
            CoreDataManager.sharedManager.save(
                isDarkMode: false,
                issues: GlobalSettings.sharedManager.editions as NSObject,
                bigTextSize: false
            )
            Theme.defaultTheme(sem: sem)
            sem.wait()
        }
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "MySettings")
        fetch.fetchLimit = 1
        let fetchSettings = try! managedContext.fetch(fetch)
        if fetchSettings.count > 0{
            let settings: MySettings = fetchSettings.first as! MySettings
            settings.issues = GlobalSettings.sharedManager.editions as NSObject
            settings.isDarkMode = GlobalSettings.sharedManager.isDarkMode
            settings.currentUser = GlobalSettings.sharedManager.currentUser
            settings.bigTextSize = GlobalSettings.sharedManager.bigTextSize
            CoreDataManager.sharedManager.saveContext()
        }
    }

}

