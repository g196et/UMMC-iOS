//
//  SearchViewController.swift
//  Diplom
//
//  Created by Константин Малахов on 17.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var posts = [NSDictionary]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var searchButton: SearchButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var labelMessage: UILabel!
    var timer: Timer!
    let SearchTableViewCell = "SearchTableViewCell"
    let postSegueIdentifier = "SearchToPostSegue"
    let topTitle = "Поиск"
    
    // MARK: - JSON
    func getJsonFromUrl(params: [String:String]){
        //creating a NSURL
        posts = [NSDictionary]()
        
        let urlComp = NSURLComponents(string: "http://kolalola-001-site1.btempurl.com/news/search")!
        
        var items = [URLQueryItem]()
        
        for (key,value) in params {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        items = items.filter{!$0.name.isEmpty}
        
        if !items.isEmpty {
            urlComp.queryItems = items
        }
        
        var urlRequest = URLRequest(url: urlComp.url!)
        urlRequest.httpMethod = "GET"
        
        let sem = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            if let dictionary = json as? [String: Any] {
                if let articles = dictionary["articles"] as? NSArray{
                    for post_json in articles {
                        let post_data = post_json as! NSDictionary
                        self.posts.append(post_data)
                    }
                }
            }
            sem.signal()
        })
        task.resume()
        sem.wait()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self as UITableViewDelegate
        tableView.dataSource = self as UITableViewDataSource
        // Do any additional setup after loading the view.
        searchButton.layer.cornerRadius = searchButton.frame.width / 2
        searchButton.clipsToBounds = true
        self.tabBarItem.selectedImage = #imageLiteral(resourceName: "ic_search_pressed")
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.topItem?.title = topTitle
        }
    }

    @IBAction func getSearch(_ sender: UIButton) {
        if (self.searchTextField.text?.count)! > 0 {
            getJsonFromUrl(params: ["text" : self.searchTextField.text!])
            self.tableView.reloadData()
        }
        if self.posts.count == 0{
            print("NOT FOUND")
            self.labelMessage.text = "Ничего не найдено :("
        }
        else{
            print("RELOAD DATA")
            self.labelMessage.text = "Результаты:"
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == self.postSegueIdentifier,
            let destination = segue.destination as? PostViewController,
            let cellIndex = tableView.indexPathForSelectedRow?.row
        {
            let post = self.posts[cellIndex]
            let id = post["id"] as! Int
            destination.idPost = id
        }
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SearchTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                       for: indexPath) as? SearchTableViewCell  else {
                                                        fatalError("The dequeued cell is not an instance of SearchTableViewCell.")
        }
        let post = self.posts[indexPath.row]
        cell.labelText.text = post["name"] as? String
//        let data = try? Data(contentsOf : post.imageUrl)
//        let image = UIImage(data : data!)
//        cell.previewImage.image = image!
//        cell.editionLabel.text = post.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }

}
