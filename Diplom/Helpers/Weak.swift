//
//  Weak.swift
//  Diplom
//
//  Created by Константин Малахов on 27.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import Foundation

/// A box that allows us to weakly hold on to an object
struct Weak<Object: AnyObject> {
    weak var value: Object?
}
