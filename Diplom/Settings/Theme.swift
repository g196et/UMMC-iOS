//
//  Theme.swift
//  Diplom
//
//  Created by g196et on 03.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import Foundation
import UIKit

struct Theme {
    
    static var backgroundColor:UIColor?
    static var navigationBarArrow: String?
    // Articles
    static var articleCellBackground: UIColor?
    static var articleContentBackground: UIColor?
    static var articleLabelsText: UIColor?
    static var articleLabelsBackground: UIColor?
    static var articleImageBackground: UIColor?
    static var browserImage: String?
    static var shareImage: String?
    // Releases
    static var releaseCellBackground: UIColor?
    static var releaseCellText: UIColor?
    static var downloadBorder: UIColor?
    static var downloadImage: String?
    static var segmentedEnableBackground: UIColor?
    static var segmentedBackground: UIColor?
    // Others
    static var buttonTextColor:UIColor?
    static var buttonBackgroundColor:UIColor?
    static var labelTextColor:UIColor?
    static var labelBackgroundColor: UIColor?
    static var navigationBarTextColor: UIColor?
    static var navigationBarBackground: UIColor?
    static var tabBarTextColor: UIColor?
    static var tabBarBackground: UIColor?
    static var searchImage: String?
    // Settings
    static var dayImage: String?
    static var nightImage: String?

    
    static public func defaultTheme(sem: DispatchSemaphore) {
        // General
        // let orange = UIColor(displayP3Red: 215, green: 91, blue: 33, alpha: 1)
        self.backgroundColor = .white
        self.navigationBarArrow = "ic_arrow_back_black_preview"
        // Articles
        self.articleCellBackground = .black
        self.articleContentBackground = .white
        self.articleLabelsBackground = .black
        self.articleImageBackground = .black
        self.articleLabelsText = .white
        self.browserImage = "ic_browser_white"
        self.shareImage = "ic_share_white"
        // Releases
        self.releaseCellBackground = .black
        self.releaseCellText = .white
        self.downloadBorder = .black
        self.downloadImage = "ic_download_black"
        self.segmentedEnableBackground = .black
        self.segmentedBackground = .white
        // Others
        self.buttonTextColor = .white
        self.buttonBackgroundColor = .black
        self.labelTextColor = .black
        self.labelBackgroundColor = .white
        self.navigationBarTextColor = .black
        self.navigationBarBackground = .white
        self.tabBarBackground = .white
        // Search
        self.searchImage = "ic_search_black"
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            updateDisplay()
        }
        sem.signal()
    }
    
    static public func darkTheme(sem: DispatchSemaphore) {
        let gray = UIColor(displayP3Red: 129, green: 129, blue: 129, alpha: 1)
        // General
        self.backgroundColor = .black
        self.navigationBarArrow = "ic_arrow_back_white_preview"
        // Articles
        self.articleCellBackground = .white
        self.articleContentBackground = .black
        self.articleLabelsBackground = .white
        self.articleImageBackground = .white
        self.articleLabelsText = .black
        self.browserImage = "browser_link"
        self.shareImage = "ic_share_black"
        // Releases
        self.releaseCellBackground = .white
        self.releaseCellText = .black
        self.downloadBorder = .white
        self.downloadImage = "ic_download_white"
        self.segmentedEnableBackground = .white
        self.segmentedBackground = .black
        // Others
        self.buttonTextColor = .black
        self.buttonBackgroundColor = .white
        self.labelTextColor = .white
        self.labelBackgroundColor = .black
        self.navigationBarTextColor = .white
        self.navigationBarBackground = .black
        self.tabBarTextColor = gray
        self.tabBarBackground = .black
        // Search
        self.searchImage = "ic_search_white"
        updateDisplay()
        sem.signal()
    }
    
    static public func updateDisplay() {
        // Общая вью
        let proxyView = GeneralView.appearance()
        proxyView.backgroundColor = backgroundColor
        
        // Кнопки в целом
//        let proxyButton = UIButton.appearance()
//        proxyButton.setTitleColor(Theme.buttonTextColor, for: .normal)
//        proxyButton.backgroundColor = Theme.buttonBackgroundColor
        
        // Лейблы в целом
        let proxyLabel = UILabel.appearance()
        proxyLabel.textColor = Theme.labelTextColor
        proxyLabel.backgroundColor = Theme.labelBackgroundColor
        
        // Tab bar
        let proxyTabBar = UITabBar.appearance()
        proxyTabBar.tintColor = UIColor(displayP3Red: 215/255, green: 91/255, blue: 33/255, alpha: 1)  // Цвет выбранного айтема в табБаре всегда оранжевый
//        proxyTabBar.backgroundColor = Theme.tabBarBackground
//        proxyTabBar.barTintColor = Theme.tabBarBackground
        proxyTabBar.unselectedItemTintColor = .gray
        
        // Navigation Bar
//        let proxyNavigationBar = UINavigationBar.appearance()
//        proxyNavigationBar.isTranslucent = true
//        proxyNavigationBar.backgroundColor = Theme.navigationBarBackground
//        proxyNavigationBar.barTintColor = Theme.navigationBarBackground
//        proxyNavigationBar.tintColor = Theme.navigationBarTextColor
//        proxyNavigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]  // Theme.navigationBarTextColor!]
        
        // UIActivityIndicatorView
        let proxyIndicator = UIActivityIndicatorView.appearance()
        proxyIndicator.tintColor = UIColor(displayP3Red: 255/255, green: 98/255, blue: 42/255, alpha: 1)
        proxyIndicator.backgroundColor = Theme.backgroundColor
        
        // UITextFiled
        let proxyTextField = UITextField.appearance()
        proxyTextField.backgroundColor = .white
        proxyTextField.tintColor = UIColor(displayP3Red: 255/255, green: 98/255, blue: 42/255, alpha: 1)
        proxyTextField.textColor = .black
        
        // Кнопки браузера и поделиться в статье
        let proxyBrowserButton = BrowserButton.appearance()
        proxyBrowserButton.setImage(UIImage(named: Theme.browserImage!), for: .normal)
        proxyBrowserButton.backgroundColor = Theme.articleImageBackground
        let proxyShareButton = ShareButton.appearance()
        proxyShareButton.setImage(UIImage(named: Theme.shareImage!), for: .normal)
        proxyShareButton.backgroundColor = Theme.articleImageBackground
        let proxyEditionButton = EditionButton.appearance()
        proxyEditionButton.backgroundColor = Theme.articleImageBackground
        proxyEditionButton.setTitleColor(Theme.backgroundColor, for: .normal)
        
        // Список статей
        let proxyArticleContent = ArticleContentView.appearance()
        proxyArticleContent.backgroundColor = Theme.articleCellBackground
        let articleFeedTableView = ArticleFeedTableView.appearance()
        articleFeedTableView.backgroundColor = Theme.backgroundColor
        articleFeedTableView.tableFooterView?.backgroundColor = Theme.backgroundColor
        articleFeedTableView.sectionIndexBackgroundColor = Theme.backgroundColor
        let proxyArticleCell = PostTableViewCell.appearance()
        proxyArticleCell.backgroundColor = Theme.articleCellBackground
        
        // Внутренность статьи
        let proxyArticle = ArticleView.appearance()
        proxyArticle.backgroundColor = Theme.articleContentBackground
        
        // Лейблы в статье
        let proxyArticleLabel = ArticleLabel.appearance()
        proxyArticleLabel.backgroundColor = Theme.articleLabelsBackground
        proxyArticleLabel.textColor = Theme.articleLabelsText
        
        // Segments (PDF или статьи)
        let proxySegmented = UISegmentedControl.appearance()
        proxySegmented.backgroundColor = Theme.segmentedBackground
        proxySegmented.tintColor = Theme.segmentedEnableBackground
        
        // Releases
        let proxyReleaseCell = ReleaseCell.appearance()
        let proxyReleaseLabels = ReleaseLabel.appearance()
        let proxyCollectionView = UICollectionView.appearance()
        let proxyReleaeseArticlesCell = ArticlesContainerCell.appearance()
        let proxyReleaseArticlesTable = ReleaseArticlesTableView.appearance()
        proxyCollectionView.backgroundColor = Theme.backgroundColor
        proxyReleaseCell.backgroundColor = Theme.releaseCellBackground
        proxyReleaseLabels.backgroundColor = Theme.releaseCellBackground
        proxyReleaseLabels.textColor = Theme.releaseCellText
        proxyReleaeseArticlesCell.backgroundColor = Theme.backgroundColor
        proxyReleaseArticlesTable.backgroundColor = Theme.backgroundColor
        
        // Issues
        let proxyIssues = IssueTableView.appearance()
        proxyIssues.backgroundColor = Theme.backgroundColor
        let proxyIssuesCells = IssueTableViewCell.appearance()
        proxyIssuesCells.backgroundColor = Theme.backgroundColor
        let proxyDownloadButton = DownloadButton.appearance()
        proxyDownloadButton.setImage(UIImage(named: Theme.downloadImage!), for: .normal)
        proxyDownloadButton.backgroundColor = Theme.backgroundColor
        let proxyYearTagButton = YearTagButton.appearance()
        proxyYearTagButton.backgroundColor = Theme.articleImageBackground
        proxyYearTagButton.setTitleColor(Theme.backgroundColor, for: .normal)
        
        // Поиск
        let proxySearchButton = SearchButton.appearance()
        let proxySearchTableCell = SearchTableViewCell.appearance()
        let proxySearchTable = SearchTableView.appearance()
        proxySearchButton.setImage(UIImage(named: Theme.searchImage!), for: .normal)
        proxySearchButton.backgroundColor = Theme.backgroundColor
        proxySearchTableCell.backgroundColor = Theme.backgroundColor
        proxySearchTable.backgroundColor = Theme.backgroundColor
        
        // Settings
        let proxyDayImage = DayButton.appearance()
        proxyDayImage.backgroundColor = .black
        let proxyNightImage = NightButton.appearance()
        proxyNightImage.backgroundColor = .white
    }
}
