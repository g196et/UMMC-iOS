//
//  SettingsViewController.swift
//  Diplom
//
//  Created by g196et on 03.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var autorizationLabel: UILabel!
    @IBOutlet weak var isBigText: UISwitch!
    @IBOutlet weak var themeSwitch: UISwitch!
    let topTitle = "Настройки"
    
    func updateView(){
        if (GlobalSettings.sharedManager.currentUser != nil && GlobalSettings.sharedManager.currentUser != "") {
            loginButton.isHidden = true
            logoutButton.isHidden = false
            autorizationLabel.text = "Вы авторизированы как: " + GlobalSettings.sharedManager.currentUser!
            self.logoutButton.layer.cornerRadius = 10
            self.logoutButton.clipsToBounds = true
        } else {
            logoutButton.isHidden = true
            loginButton.isHidden = false
            autorizationLabel.text = "Авторизируйтесь, чтобы открыть расширенные настройки"
            self.loginButton.layer.cornerRadius = 10
            self.loginButton.clipsToBounds = true
        }
        if GlobalSettings.sharedManager.bigTextSize {
            isBigText.isOn = true
        } else {
            isBigText.isOn = false
        }
        if GlobalSettings.sharedManager.isDarkMode {
            themeSwitch.isOn = true
        } else {
            themeSwitch.isOn = false
        }
    }
    
    @IBAction func logout(_ sender: UIButton) {
        GlobalSettings.sharedManager.currentUser = nil
        updateView()
    }
    
    @IBAction func changeTheme(_ sender: UISwitch) {
        let sem = DispatchSemaphore(value: 0)
        if themeSwitch.isOn {
            GlobalSettings.sharedManager.isDarkMode = true
            Theme.darkTheme(sem: sem)
            self.loadView()
            self.updateView()
            sem.wait()
        } else {
            GlobalSettings.sharedManager.isDarkMode = false
            Theme.defaultTheme(sem: sem)
            self.loadView()
            self.updateView()
            sem.wait()
        }
//        self.view.setNeedsLayout()
//        self.view.layoutIfNeeded()
    }
    
    @IBAction func switchFontSize(_ sender: UISwitch) {
        let proxy = TextLabel.appearance()
        if sender.isOn {
            GlobalSettings.sharedManager.bigTextSize = true
            proxy.font = UIFont.systemFont(ofSize: 22)
        }
        else{
            GlobalSettings.sharedManager.bigTextSize = false
            proxy.font = UIFont.systemFont(ofSize: 17)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.topItem?.title = topTitle
        }
        updateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
