//
//  GlobalSettings.swift
//  Diplom
//
//  Created by Константин Малахов on 20.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import Foundation

class GlobalSettings {
    
    var editions: [Int]
    var isDarkMode: Bool
    var bigTextSize: Bool
    var currentUser: String?
    
    init(){
        self.editions = [Int]()
        self.isDarkMode = false
        self.bigTextSize = false
    }
    
    class var sharedManager: GlobalSettings {
        struct Static {
            static let instance = GlobalSettings()
        }
        return Static.instance
    }
}
