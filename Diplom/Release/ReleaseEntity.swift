//
//  ReleaseEntity.swift
//  Diplom
//
//  Created by Константин Малахов on 26.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import Foundation
import UIKit

struct ReleaseShort: Decodable {
    var id: Int
    var number: String
    var date: Date
    var image: String?
    
    init(id: Int, number: String, date: Date, image: String?){
        self.id = id
        self.number = number
        self.date = date
        self.image = image
    }
}

struct ReleaseFull: Decodable {
    var id: Int
    var number: String
    var date: String
    var image: String?
    var pdf: String?
    var editionName: String
    var articles: [PostShort]
    
    init(id: Int, number: String, date: String, image: String?, pdf: String?, editionName: String, articles: [PostShort]){
        self.id = id
        self.number = number
        self.date = date
        self.image = image
        self.pdf = pdf
        self.editionName = editionName
        self.articles = articles
    }
    
    init() {
        self.id = 0
        self.number = ""
        self.date = ""
        self.image = nil
        self.pdf = nil
        self.editionName = ""
        self.articles = []
    }
}
