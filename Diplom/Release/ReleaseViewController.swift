//
//  ReleaseViewController.swift
//  Diplom
//
//  Created by Константин Малахов on 26.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

final class ReleaseViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet var downloadButton: UIButton!
    @IBOutlet var collectionView: UIView!
    var idRelease = Int()
    var release = [ReleaseFull]()
    
    @IBOutlet var segmentedControl: UISegmentedControl!
    
    @IBAction func downloadFile(_ sender: Any) {
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let pdfName = self.release[0].pdf!.components(separatedBy: "/")
        let destinationFileUrl = documentsUrl.appendingPathComponent(pdfName.last!)
        
        let url = URL(string: "http://kolalola-001-site1.btempurl.com" + self.release[0].pdf!)
        print(url!)
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        var request = URLRequest(url:url!)
        request.httpMethod = "GET"
        
        let alert = UIAlertController(title: "", message: "Скачивание завершено", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
            }
        }))
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {

                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                do {
                    let isExist = try FileManager.default.fileExists(atPath: destinationFileUrl.absoluteString)
                    print(isExist)
                    if isExist {
                        try FileManager.default.removeItem(atPath: destinationFileUrl.absoluteString)
                    }
                }
                catch(let _){
                    print("Что-то пошло не так")
                }
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    self.present(alert, animated: true, completion: nil)
                } catch (let writeError) {
                    alert.message = "Что-то пошло не так"
                    self.present(alert, animated: true, completion: nil)
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
                
            } else {
                print("Error took place while downloading a file. Error description: %@", error?.localizedDescription ?? "Error");
            }
        }
        task.resume()
        
    }
    
    private lazy var PDFController: PDFViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "PDFViewController") as! PDFViewController
        viewController.path = ("http://kolalola-001-site1.btempurl.com" + self.release[0].pdf!)
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var articlesController: ArticlesContainerController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "ArticlesContainerController") as! ArticlesContainerController
        viewController.articles = self.release[0].articles
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    // MARK: - JSON
    let URL_CONST = "http://kolalola-001-site1.btempurl.com/releases/getrelease?id="
    
    //this function is fetching the json from URL
    func getJsonFromUrl(id: Int){
        //creating a NSURL
        guard let url = URL(string: self.URL_CONST + String(id)) else { return }
        let sem = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            let decoder = JSONDecoder()
            if let dictionary = json as? [String: Any] {
                if let release = dictionary["release"] as? NSDictionary {
                    let jsonData = try? JSONSerialization.data(withJSONObject: release, options: [])
                    let releaseModel = try! decoder.decode(ReleaseFull.self, from: jsonData!)
                    self.release.append(releaseModel)
                }
            }
            sem.signal()
        })
        task.resume()
        sem.wait()
    }
    
    // MARK: - Segmentation controll
    
    private func updateView() {
        if self.segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: self.articlesController)
            add(asChildViewController: self.PDFController)
        }
        else {
            remove(asChildViewController: self.PDFController)
            add(asChildViewController: self.articlesController)
        }
    }
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        self.collectionView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.collectionView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    @objc func selectionDidChange(_ sender: UISegmentedControl) {
        updateView()
    }
    private func setupSegmentedControl() {
        // Configure Segmented Control
        self.segmentedControl.removeAllSegments()
        self.segmentedControl.insertSegment(withTitle: "PDF", at: 0, animated: false)
        self.segmentedControl.insertSegment(withTitle: "Статьи", at: 1, animated: true)
        self.segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        
        // Select First Segment
        self.segmentedControl.selectedSegmentIndex = 0
    }
    private func setupView() {
        setupSegmentedControl()
        updateView()
    }
    
    // MARK: - ViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        getJsonFromUrl(id: self.idRelease)
        downloadButton.layer.cornerRadius = downloadButton.frame.width / 2
        downloadButton.clipsToBounds = true
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = self.release[0].editionName + "№ " + self.release[0].number
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
