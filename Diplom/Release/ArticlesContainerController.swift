//
//  ArticlesContainerController.swift
//  Diplom
//
//  Created by Константин Малахов on 26.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class ArticlesContainerController: UITableViewController {

    var articles = [PostShort]()
    let articleSegueIdentifier = "ReleaseToArticleSegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.articles.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticlesContainerCell", for: indexPath) as! ArticlesContainerCell
        let article = articles[indexPath.row]
        let imageUrl = URL(string: ("http://kolalola-001-site1.btempurl.com" + article.image))
        let data = try? Data(contentsOf : imageUrl!)
        cell.imageArticle.image = UIImage(data : data!)
        cell.title.text = article.name
        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == self.articleSegueIdentifier,
            let destination = segue.destination as? PostViewController,
            let cellIndex = tableView.indexPathForSelectedRow?.row
        {
            let post = self.articles[cellIndex]
            destination.idPost = post.id
        }
    }

}
