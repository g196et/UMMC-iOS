//
//  ArticlesContainerCell.swift
//  Diplom
//
//  Created by Константин Малахов on 27.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class ArticlesContainerCell: UITableViewCell {

    @IBOutlet var imageArticle: UIImageView!
    @IBOutlet var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
