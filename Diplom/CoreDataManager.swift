//
//  CoreDataManager.swift
//  Diplom
//
//  Created by Константин Малахов on 13.06.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    
    //1
    static let sharedManager = CoreDataManager()
    //2.
    private init() {} // Prevent clients from creating another instance.
    
    //3
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Diplom")
        
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    //4
    func saveContext () {
        let context = CoreDataManager.sharedManager.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func save(isDarkMode: Bool, issues : NSObject?, bigTextSize: Bool) {
        
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "MySettings",
                                                in: managedContext)!
        
        let settings = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        settings.setValue(isDarkMode, forKeyPath: "isDarkMode")
        settings.setValue(issues, forKeyPath: "issues")
        settings.setValue(bigTextSize, forKeyPath: "bigTextSize")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func update(isDarkMode: Bool, issues : NSObject?, settings : MySettings, bigTextSize: Bool) {

        let context = CoreDataManager.sharedManager.persistentContainer.viewContext
        
        do {
            settings.setValue(isDarkMode, forKey: "isDarkMode")
            settings.setValue(issues, forKey: "issues")
            settings.setValue(bigTextSize, forKey: "bigTextSize")
            
            print("\(String(describing: settings.value(forKey: "isDarkMode")))")
            print("\(String(describing: settings.value(forKey: "issues")))")
            
            do {
                try context.save()
                print("saved!")
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            } catch {
                
            }
            
        } catch {
            print("Error with request: \(error)")
        }
    }
}
