//
//  LoginViewController.swift
//  Diplom
//
//  Created by Константин Малахов on 29.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var forgotPassButton: UIButton!
    @IBOutlet var registrationButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    
    @IBAction func login(_ sender: UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.submitPost(email: self.email.text!, password: self.password.text!)
        }
    }
    
    func submitPost(email: String, password: String){
        var dictionary = [String: String]()
        dictionary["email"] = email
        dictionary["password"] = password
        let url = NSURLComponents(string: "http://kolalola-001-site1.btempurl.com/authorization/login")!
        
        var urlRequest = URLRequest(url: url.url!)
        urlRequest.httpMethod = "POST"
        
        var headers = urlRequest.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        urlRequest.allHTTPHeaderFields = headers
        
        let encoder = JSONEncoder()
        urlRequest.httpBody = try? encoder.encode(dictionary)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: urlRequest) { (responseData, response, responseError) in
            if let data = responseData {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let dictionary = json as? [String: Any] {
                    print(dictionary["success"] ?? "test")
                    if dictionary["success"] as! Bool == false {
                        let alert = UIAlertController(
                            title: "Ошибка авторизации",
                            message: dictionary["message"] as? String,
                            preferredStyle: UIAlertControllerStyle.alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else {
                        GlobalSettings.sharedManager.currentUser = email
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            } else {
                print("no readable data received in response")
            }
        }
        task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.forgotPassButton.layer.cornerRadius = 10
        self.forgotPassButton.clipsToBounds = true
        self.registrationButton.layer.cornerRadius = 10
        self.registrationButton.clipsToBounds = true
        self.loginButton.layer.cornerRadius = 10
        self.loginButton.clipsToBounds = true
        self.email.attributedText = NSAttributedString(
            string: self.email.placeholder != nil ? self.email.placeholder! : "",
            attributes: [NSAttributedStringKey.strikethroughColor: UIColor.white]
        )
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
