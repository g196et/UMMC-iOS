//
//  RegistrationViewController.swift
//  Diplom
//
//  Created by Константин Малахов on 29.05.2018.
//  Copyright © 2018 g196et. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet var email: UITextField!
    @IBOutlet var telephone: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var passwordRepeat: UITextField!
    @IBOutlet var registrationButton: UIButton!
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Other
    // TODO: Валидация полей
    @IBAction func registration(_ sender: Any) {
        if (email.text?.isEmpty == true || password.text?.isEmpty == true || passwordRepeat.text?.isEmpty == true) {
            showAlert(title: "Ошибка регистрации", message: "Необходимо заполнить обязательные поля!")
        }
        else if (password.text != passwordRepeat.text){
            showAlert(title: "Ошибка регистрации", message: "Пароли должны совпадать!")
        }
        else {
            submitPost(email: email.text!, password: password.text!)
        }
    }
    
    func submitPost(email: String, password: String){
        var dictionary = [String: String]()
        dictionary["email"] = email
        dictionary["password"] = password
        let url = NSURLComponents(string: "http://kolalola-001-site1.btempurl.com/authorization/register")!

        var urlRequest = URLRequest(url: url.url!)
        urlRequest.httpMethod = "POST"
        
        var headers = urlRequest.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        urlRequest.allHTTPHeaderFields = headers
        
        let encoder = JSONEncoder()
        urlRequest.httpBody = try? encoder.encode(dictionary)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: urlRequest) { (responseData, response, responseError) in
            // APIs usually respond with the data you just sent in your POST request
            if let data = responseData {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let dictionary = json as? [String: Any] {
                    print(dictionary["success"] ?? "test")
                    if dictionary["success"] as! Bool == false {
                        self.showAlert(title: "Ошибка регистрации", message: (dictionary["message"] as? String)!)
                    }
                    else {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(
                                title: "Успешная регистрация",
                                message: dictionary["message"] as? String,
                                preferredStyle: UIAlertControllerStyle.alert
                            )
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    self.navigationController?.popViewController(animated: true)
                                case .cancel:
                                    self.navigationController?.popViewController(animated: true)
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            } else {
                print("no readable data received in response")
            }
        }
        task.resume()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.registrationButton.layer.cornerRadius = 10
        self.registrationButton.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
